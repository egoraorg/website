---
title: Egora Chat Launch
subtitle: We are proud to announce the launch of Egora Chat.
date: 2020-07-13
tags: ["news"]
---

The internet has strayed from its original mission. No longer is it a place where freedom of association and free speech rules supreme, it is now a walled garden controlled by politically motivated tech giants.

<!--more-->

## Why are we launching a new chat platform?
Egora is founded by a politically right-wing international coalition of software developers, community managers, and content creators. Most of our communities have been run on Discord, which has served us well(ish) up to this point. However, over the past year and a half, Discord has taken a drastic turn for the worse. Their guidelines for communities have always been hazy, but now running a community, even being a user on their platform, has become insufferable.

## What do you mean insufferable?
Discord has always been very vague about their moderation guidelines, leaving community owners to guess where the limits of free speech are on their platform. We had more or less figured out how to deal with this through rules and moderation in our communities. It was mostly simple stuff, disallow the worst of slurs, delete content that was too spicy, and so on... This has now changed and the rules for what Discord will delete a community over have expanded to a point where it basically covers everything that does not fall in line with current intersectional dogma.

Any community with members who are not fully on board with the LGBT and BLM movements are apparently engaging in hate speech, and will be removed from the platform. Not only do they punish those individual users, they threaten to delete the entire community, they refuse to point to which messages they want deleted, they leave you with the only option being to delete all channels and starting over.

On top of this, they even ban every user that were part of the community, whether or not the user had said anything against Discord’s ToS, or even posted anything in the community at all.

We can not live with this anymore. Last year we saw this on the horizon and started looking into alternatives. Now we have bitten the bullet, and are doing something about it.

## How do you even solve this?
Conceptually, solving this it is pretty simple, you need to build an alternative. However, this is where the simplicity ends.

### You need the right team
Running a chat service is not simple, you need a whole team of developers and DevOps specialists to operate a large-scale chat platform. Starting last year, we have been convincing people that this problem is one that would have to be tackled, and that since no one else appeared to be doing it, that we would have to.  It took quite a while to convince people of the urgency of the task, but in the past few months the issue has become clearer than ever – we now have a 10-man development team.

### You need the software
Initially, when we started looking to migrate, we found Matrix/Riot to be lacking in functionality necessary to run communities with thousands of members, but today the situation has notably improved on that front. So far so good.

### You need the funding
We need funds to run this long term, which means there has to be a business model. We have been working on this through the past year in preparation to start Egora, and we are now confident that we have a solid business model laid out. This will be based on integrating tipping features across all of the services Egora operates. More to come on this in the future.

### You need critical mass
Finally, you need to actually convince people to move over, to convince you to use what we’ve made. Now as communities with tens of thousands of members are feeling threatened with deletion, they are keen to find an alternative platform without this worry. We believe that we have the critical mass of users necessary to bring this to market and run it indefinitely. As we bring new services like Peertube and Mastodon online, we’ll have created a holistic communication and media experience with integrated tipping services, that will be a compelling product which can move over people from Discord and other platforms as well.

## Join us
If you are interested in what we are doing, please get in contact by joining Egora Chat, where you can talk to us in the Egora Support channel, which you can find in the public room explorer. 
