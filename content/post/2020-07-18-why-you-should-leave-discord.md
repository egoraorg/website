---
title: Why You Should Leave Discord
date: 2020-07-18
tags: ["news"]
---

Over the last few years, we've seen the decline of the ability to speak freely about a full range of topics on Discord. The earliest interventions came widely publicized portraying the censorship as "targeting extremists", and to Discord's credit it should be noted that the earliest waves of bans did mostly remove people outside the political norm. Whether all of those were extremists.. well, I will leave that up to you.

<!--more-->

These days censorship on social media is not even mentioned in the traditional media. It's just the norm and, especially on Discord, we can only speculate about why this is happening, while censorship of discussions outside the WHO narrative on Covid-19 or articles about investment to the tune of $150M from China into Discord dominate the picture.

It doesn't end there though. Many have reported that Discord are sending warnings to community owners to delete "harmful and threathening things in their communities" - without specifying which messages they are referring to - resulting in community owners deleting their communities and rebuilding from scratch.

The latest straw was a blogpost by Discord themselves making it clear that they would focus on catering to a broader userbase, while eliminating features that were important to their original userbase. Now I am not myself much of a representative for the original userbase, but even I can see the frustration of having thrown hundreds if not thousands of dollars at Discord, only to be left at the wayside.

Here at Egora, we are doing things differently so you won't be in the same situation in the future with your community, check out our About page to figure out what ;)

Sources:
August 14, 2017 - https://techcrunch.com/2017/08/14/discord-shuts-down-alt-right-server-and-accounts-for-tos-violations/
> The company announced the enforcement action on Twitter, emphasizing that it is “about positivity and inclusivity. Not hate. Not violence.”

Feb 28, 2018 - https://www.theverge.com/2018/2/28/17062554/discord-alt-right-neo-nazi-white-supremacy-atomwaffen
> Discord has recently shut down several notable alt-right, white nationalist servers as it attempts to purge toxic content, as reported by Polygon.

December 23, 2018 - https://www.digitaltrends.com/gaming/dicord-funding-150million/
> And now Discord has announced that it has secured $150 million in funding, with a valuation of $2.05 billion.

June 30, 2020 - https://www.digitaltrends.com/gaming/discord-pivot-away-from-gaming/
> Popular gaming chat app Discord is pivoting away from video games and rebranding as a place to talk and build relationships for everyone, not just gamers. CEO Jason Citron announced the move in a blog post and detailed that its new slogan is “Your place to talk.”

> “Games are what brought many of you on the platform, and we’ll always be grateful for that. As time passed, a lot of you realized, and vocalized, that you simply wanted a place designed to hang out and talk in the comfort of your own communities and friends,” Citron said.
