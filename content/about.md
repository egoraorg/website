---
title: About us
subtitle: Enabling freedom of association and free speech.
comments: false
---

Founded in 2020, Egora is an international Decentralized Autonomous Organization (DAO) that operates communication and media servers in federated networks.

### Foundational principles
- Freedom of Association (and Disassociation).
- Freedom of Speech.
- Privacy.
- Stability.

# Mission
Egora's mission is to enable freedom of association and free speech online.

# Vision
Egora aims bring about a world where online freedom of association and free speech is inalienable.

# Why?
For too long, a small number of ideologically identical big tech companies have dominated the services used by the majority of people on the internet. As these companies have grown, they have become more distant from their end users, and from their founding principles. They've shown that they no longer have an interest in upholding an open internet to all.

# What is it?
Egora provides an alternative to services such as Discord, Twitter and Youtube, using open source federated standards and protocols. Hosted on multiple cloud platforms for high availability, and resilience against both cyber attacks and activist activities.

You can today set up communities on Egora's chat platform the same way you can on Discord, but without big tech oversight and censorship. You will in the future also be able to connect to Mastodon (a Twitter alternative) through your Egora account, and be able to host your videos on our Peertube server (a Youtube alternative), as well as livestream content.

# Team
Egora is an international coalition of software developers, community managers, and content creators that are all tired of silicon valley hegemony. Our development team have experience ranging from fintech and crypto, to devops, and distributed systems engineering. Our community managers operate communities with tens of thousands of members, and our content creators have hundreds of thousands of subscribers. 
