---
title: Sign-up tutorial
subtitle: Tutorial for creating an account on Egora.org's Matrix Server
tags: ["tutorial"]
comments: false
---

### 1. Go to https://riot.im and either click Launch Now On Your Browser, or download and install an app.
For the purpose of the tutorial we will assume you are using the web client, but desktop is the same, and mobile apps work the same way but look a bit different.

{{< img-modal src="https://egora.org/tutorials/img/create-user/createUser0.png" alt="Open App" title="Open App">}}

### 2. Click Create Account.

{{< img-modal src="https://egora.org/tutorials/img/create-user/createUser1.png" alt="Create Account" title="Create Account">}}

### 3. Click Other and insert the link to the server: https://matrix.egora.org, then click Next.
This is important as otherwise your account will be created on Matrix.org's server rather than Egora.org.

Remember to include the https://

{{< img-modal src="https://egora.org/tutorials/img/create-user/createUser2.png" alt="Click Other and Insert Server Link" title="Click Other and Insert Server Link">}}

### 4. Enter a username and password and click Register.

{{< img-modal src="https://egora.org/tutorials/img/create-user/createUser3.png" alt="Enter Username and Password" title="Enter Username and Password">}}

### 5. You will then be asked to set up a recovery passphrase.
It is highly recommended that you do so as if you do not you will not be able to send or receive encrypted messages.
After you enter it the first time and click continue, you will be ask to repeat it.

{{< img-modal src="https://egora.org/tutorials/img/create-user/createUser4.png" alt="Enter Recovery Passphrase" title="Enter Recovery Passphrase">}}

### 6. You will be asked to save a recovery key, click download or copy to continue.
Please do so and back it up to ensure you can regain access to encrypted messages in case you forget your recovery passphrase.

{{< img-modal src="https://egora.org/tutorials/img/create-user/createUser5.png" alt="Save Recovery Phrase" title="Save Recovery Phrase">}}

### 7. Now you can sign in. For a tutorial on how to, click the following link:
[Sign In Tutorial](https://gitlab.com/egoraorg/tutorials/-/blob/master/sign-in.md)
