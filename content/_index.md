## Are you tired of censorship? We are.

We are so tired of censorship that for the past year we have been looking for alternatives. And when we found none, we decided to start building our own.

Fortunately, there is a movement in the open source software community which is building federated alternatives to everything from Discord, to Youtube and Twitter. 

Unfortunately, a lot of them have the same censorious tendencies, so, Egora.org is going to be hosting services where free speech is the central ethos.
