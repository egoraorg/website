---
title: FAQ
subtitle: Frequently Asked Questions.
comments: false
---

# What is Egora?
Egora is a federated communication and media services provider. It's easy to use, open source, free, and secure.

## Why Egora?
We created Egora because we are tired of censorship on major platforms like Discord, Twitter and YouTube. Egora is a free speech oriented initiative with focus on privacy, security, and freedom of association.

## What does federated mean?
Federation is a form of decentralization, in federated networks there are multiple server operators instead of one. In the case of platforms like Discord, Twitter, Youtube etc. there is a single organization running one centralized platform, whereas with federated systems it is a network of many organizations running each their own servers.

## What does federation mean for you?
As a user in a federated network you can communicate not only with users on the server you are signed up on, but with users signed up on other servers as well. This allows you to move to a different server if you get tired of Egora, without losing connection with your friends and communities on Egora. This is in stark contrast to centralized platforms where opting out of the service will prevent you from accessing anything on the platform.

## What services do Egora operate?
Egora aims to provide federated alternatives to many popular communication and media services such as Discord, Twitter, YouTube and others. Right now we operate a Matrix protocol server (Synapse), and a chat client based on Riot, which is a federated Discord alternative with thousands of servers in the network. In the future we will be operating other services too, such as Mastodon (a federated Twitter alternative), and PeerTube (a federated YouTube alternative).

## What is Matrix & Synapse?
Matrix is a communication protocol specification, and Synapse is the server implementation of Matrix. Matrix provides realtime chat functionality with support for communities like "Discord Servers". Matrix and Synapse are open source and developed by New Vector and the Matrix Foundation.

## What is Riot?
Riot is the client for connecting to Matrix Synapse. Riot is available to be used in your web browser, downloaded to your deskop/laptop, or as an app on Apple store or the Google Play store.

## Is Egora secure?
Egora will be hosted on multiple different cloud platforms, creating a service resiliant against both generic cyber attacks, as well as takedowns from government actors or actitivist groups.

Egora allows for its users to create encrypted rooms. The encrpytion uses asymmetric eliptic curve cryptography, and is fully end-to-end (user to user). Communications shared in these rooms will only be readable by people who have been invited to join - communications will not be able to be read by anyone else, including Egora, or the Matrix teams.

Matrix is used by the French Government for all communications between ministries, and has been security audited by the French Government's security agency.

## Future Features
### Voice chat - ETA: Q3 2020
Voice chat integration equivalent of Discord voice chat, with support for push to talk.

### Tipping integration - ETA: Q4 2020
Users will be able to tip each other, using crypto currencies, as well as digital fiat currencies.

### Mastodon - ETA: Q1 2021
Twitter alternative.

### Peertube - ETA: Q2 2021
Youtube alternative.
